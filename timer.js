// https://www.sitepoint.com/build-javascript-countdown-timer-no-dependencies/
// Specs: 1) Set a valid end date. 2) Calculate the time remaining. 3) Convert the time to a usable format. 4) Output the clock data as a reusable object. 5) Display the clock on the page, and stop the clock when it reaches zero.


function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor( (t/1000) % 60 );
  var minutes = Math.floor( (t/1000/60) % 60 );
  var hours = Math.floor( (t/(1000*60*60)) % 24 );
  var days = Math.floor( t/(1000*60*60*24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id, endtime) {
    var clock = document.getElementById(id);
    var daysSpan = clock.querySelector('.days');
    var hoursSpan = clock.querySelector('.hours');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');
  
function updateClock() {
    var t = getTimeRemaining(endtime);
        
            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2); // instead of only t.hours in order to add an 0 before the number
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2); // the same as above
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2); // the same as above
    
  if(t.total<=0){
  clearInterval(timeinterval);
  }
}

updateClock(); // run function once at first to avoid delay
var timeinterval = setInterval(updateClock,1000);

}

var deadline = new Date(Date.parse(new Date('March 31 2018 23:59:59 GMT+0100'))); // here you are defining the deadline, this code takes only this format 
    // new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000); - this was in the original code
initializeClock('clockdiv', deadline);
